﻿namespace Cargo4You.API.Models.Package
{
    public class UpdatePackageAPI
    {
        public string? Note { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public double Weight { get; set; }
        public int? DeliveryOptionId { get; set; }
    }
}