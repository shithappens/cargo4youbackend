﻿using AutoMapper;
using Cargo4You.API.Models.Package;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Models.Package;
using Cargo4You.Application.Models.PackageInfo;
using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Cargo4You.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackagesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IPackageService packageService;
        private readonly IMapper mapper;

        public PackagesController(IUnitOfWork unitOfWork, IPackageService packageService, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.packageService = packageService;
            this.mapper = mapper;
        }

        // GET: api/<PackagesController>
        [HttpGet("deliveryStates")]
        public IEnumerable<DeliveryState> Get()
        {
            return unitOfWork.DeliveryStateRepository.FindAll().ToList();
        }

        [HttpPost]
        public IActionResult CreatePackage()
        {
            try
            {
                return Ok(packageService.CreatePackage());
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackage(Guid id, UpdatePackageAPI request)
        {
            try
            {
                var updateDTO = mapper.Map<UpdatePackageDTO>(request);
                updateDTO.EncodedId = id;
                return Ok(await packageService.UpdatePackageAsync(updateDTO));
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        // GET api/<PackagesController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                return Ok(await packageService.GetPackageAsync(id));
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpGet("{id}/sender")]
        public async Task<IActionResult> GetSender(Guid id)
        {
            try
            {
                return Ok(await packageService.GetSenderInfoAsync(id));
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpGet("{id}/receiver")]
        public async Task<IActionResult> GetReceiver(Guid id)
        {
            try
            {
                return Ok(await packageService.GetRecipientInfoAsync(id));
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpPut("{id}/receiver")]
        public async Task<IActionResult> PutReceived(Guid id, PackageInfoDTO packageInfoDTO)
        {
            try
            {
                await packageService.UpdatePackageInfoAsync(id, packageInfoDTO);
                return Ok();
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpPut("{id}/sender")]
        public async Task<IActionResult> PutSender(Guid id, PackageInfoDTO packageInfoDTO)
        {
            try
            {
                await packageService.UpdatePackageInfoAsync(id, packageInfoDTO);
                return Ok();
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }

        [HttpGet("{id}/deliveryOptions")]
        public async Task<IActionResult> GetDeliveryOptions(Guid id)
        {
            try
            {
                var res = await packageService.GetDeliveryOptionsAsync(id, true);
                return Ok(res);
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }
    }
}