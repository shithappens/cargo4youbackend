﻿using AutoMapper;
using Cargo4You.API.Models.Package;
using Cargo4You.Application.Models.Package;

namespace Cargo4You.API.Mappers
{
    public class DeliveryAPIProfile : Profile
    {
        public DeliveryAPIProfile()
        {
            CreateMap<UpdatePackageAPI, UpdatePackageDTO>().ReverseMap();
        }
    }
}