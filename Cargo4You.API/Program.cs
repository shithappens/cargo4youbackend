using Cargo4You.API.Extensions;
using Cargo4You.API.Mappers;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Mappers;
using Cargo4You.Application.Services;
using Cargo4You.Infrastructure;
using Cargo4You.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Http.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.ConfigureDatabase(builder.Configuration);
builder.Services.AddAutoMapper(typeof(DeliveryDTOProfile), typeof(DeliveryAPIProfile));
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IPackageService, PackageService>();
builder.Services.AddScoped<IPaymentService, PaymentService>();
builder.Services.AddScoped<IDeliveryPartnerService, Cargo4YouDeliveryService>();
builder.Services.AddScoped<IDeliveryPartnerService, MaltaShipService>();
builder.Services.AddScoped<IDeliveryPartnerService, ShipFasterService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
{
    builder.AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader();
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.UseCors("MyPolicy");

app.Run();