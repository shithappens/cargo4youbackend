﻿using Cargo4You.Infrastructure.Interfaces;
using Cargo4You.Infrastructure.Repositories;

namespace Cargo4You.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext dbContext;
        private IDeliveryOptionRepository deliveryOptionRepository;
        private IDeliveryPartnerRepository deliveryPartnerRepository;
        private IDeliveryStateRepository deliveryStateRepository;
        private IPackageHistoryRepository packageHistoryRepository;
        private IPackageInfoRepository packageInfoRepository;
        private IPackageRepository packageRepository;

        public UnitOfWork(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IDeliveryOptionRepository DeliveryOptionRepository
        {
            get
            {
                if (deliveryOptionRepository == null)
                {
                    deliveryOptionRepository = new DeliveryOptionRepository(dbContext);
                }
                return deliveryOptionRepository;
            }
        }

        public IDeliveryPartnerRepository DeliveryPartnerRepository
        {
            get
            {
                if (deliveryPartnerRepository == null)
                {
                    deliveryPartnerRepository = new DeliveryPartnerRepository(dbContext);
                }
                return deliveryPartnerRepository;
            }
        }

        public IDeliveryStateRepository DeliveryStateRepository
        {
            get
            {
                if (deliveryStateRepository == null)
                {
                    deliveryStateRepository = new DeliveryStateRepository(dbContext);
                }
                return deliveryStateRepository;
            }
        }

        public IPackageHistoryRepository PackageHistoryRepository
        {
            get
            {
                if (packageHistoryRepository == null)
                {
                    packageHistoryRepository = new PackageHistoryRepository(dbContext);
                }
                return packageHistoryRepository;
            }
        }

        public IPackageInfoRepository PackageInfoRepository
        {
            get
            {
                if (packageInfoRepository == null)
                {
                    packageInfoRepository = new PackageInfoRepository(dbContext);
                }
                return packageInfoRepository;
            }
        }

        public IPackageRepository PackageRepository
        {
            get
            {
                if (packageRepository == null)
                {
                    packageRepository = new PackageRepository(dbContext);
                }
                return packageRepository;
            }
        }

        public void Save()
        {
            dbContext.SaveChanges();
            dbContext.ChangeTracker.Clear();
        }
    }
}