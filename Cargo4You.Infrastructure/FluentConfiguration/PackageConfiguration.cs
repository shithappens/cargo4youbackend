﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class PackageConfiguration : IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.Property(p => p.EncodedId)
                .IsRequired();
            builder.HasOne(p => p.Recipient).WithOne().HasForeignKey<Package>(p => p.RecipientId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.Sender).WithOne().HasForeignKey<Package>(p => p.SenderId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(p => p.DeliveryOption).WithOne().HasForeignKey<Package>(p => p.DeliveryOptionId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}