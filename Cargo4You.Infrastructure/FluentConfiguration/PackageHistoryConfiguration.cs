﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class PackageHistoryConfiguration : IEntityTypeConfiguration<PackageHistory>
    {
        public void Configure(EntityTypeBuilder<PackageHistory> builder)
        {
            builder
                .HasOne(packageHistory => packageHistory.Package)
                .WithMany(package => package.PackageHistories)
                .HasForeignKey(packageHistory => packageHistory.PackageId);
            builder
                .HasOne(packageHistory => packageHistory.DeliveryState)
                .WithMany(deliveryState => deliveryState.PackageHistories)
                .HasForeignKey(packageHistory => packageHistory.DeliveryStateId);
        }
    }
}