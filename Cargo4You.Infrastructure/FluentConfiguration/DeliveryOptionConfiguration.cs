﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class DeliveryOptionConfiguration : IEntityTypeConfiguration<DeliveryOption>
    {
        public void Configure(EntityTypeBuilder<DeliveryOption> builder)
        {
            builder.Property(p => p.DeliveryPartnerId)
                .IsRequired();
            builder.Property(p => p.Price)
                .IsRequired();
            builder.Property(p => p.PackageId)
                .IsRequired();
            builder
                .HasOne(delop => delop.Package)
                .WithMany(package => package.AvailableDeliveryOptions)
                .HasForeignKey(delop => delop.PackageId);
            builder
                .HasOne(delop => delop.DeliveryPartner)
                .WithMany(deliveryPartner => deliveryPartner.DeliveryOptions)
                .HasForeignKey(bc => bc.DeliveryPartnerId);
        }
    }
}