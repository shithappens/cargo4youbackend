﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class PackageInfoConfiguration : IEntityTypeConfiguration<PackageInfo>
    {
        public void Configure(EntityTypeBuilder<PackageInfo> builder)
        {
        }
    }
}