﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class DeliveryPartnerConfiguration : IEntityTypeConfiguration<DeliveryPartner>
    {
        public void Configure(EntityTypeBuilder<DeliveryPartner> builder)
        {
            builder.Property(p => p.Name)
                .HasMaxLength(256)
                .IsRequired();
            builder.HasData(new DeliveryPartner
            {
                Id = 1,
                Name = "Cargo4You",
            },
            new DeliveryPartner
            {
                Id = 2,
                Name = "ShipFaster",
            },
            new DeliveryPartner
            {
                Id = 3,
                Name = "MaltaShip",
            });
        }
    }
}