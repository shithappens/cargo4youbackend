﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cargo4You.Infrastructure.FluentConfiguration
{
    public class DeliveryStateConfiguration : IEntityTypeConfiguration<DeliveryState>
    {
        public void Configure(EntityTypeBuilder<DeliveryState> builder)
        {
            int increment = 1;
            builder.Property(p => p.Name)
                .HasMaxLength(256)
                .IsRequired();
            builder.HasData(new DeliveryState
            {
                Id = increment++,
                Name = "Pending",
                StatusCode = 100,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "On Hold",
                StatusCode = 101,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Accepted",
                StatusCode = 201,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Picked up",
                StatusCode = 202,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "On Transit",
                StatusCode = 203,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Delivered",
                StatusCode = 204,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Damaged",
                StatusCode = 300,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Lost",
                StatusCode = 301,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Wrong Delivery",
                StatusCode = 302,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Error",
                StatusCode = 303,
            },
            new DeliveryState
            {
                Id = increment++,
                Name = "Missing Informations",
                StatusCode = 303,
            }
            );
        }
    }
}