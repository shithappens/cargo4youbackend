﻿using Cargo4You.Domain.Models;

namespace Cargo4You.Infrastructure.Interfaces
{
    public interface IDeliveryStateRepository : IRepositoryBase<DeliveryState>
    {
    }
}