﻿using System.Linq.Expressions;

namespace Cargo4You.Infrastructure.Interfaces
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> FindAll();

        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);

        T FindById(object id);

        void Create(T entity);

        void CreateRange(IEnumerable<T> entities);

        void Update(T entity);

        void Delete(T entity);
    }
}