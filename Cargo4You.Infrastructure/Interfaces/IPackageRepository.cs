﻿using Cargo4You.Domain.Models;

namespace Cargo4You.Infrastructure.Interfaces
{
    public interface IPackageRepository : IRepositoryBase<Package>
    {
        Task<Package> GetByEncodedIdAsync(Guid encodedId);
    }
}