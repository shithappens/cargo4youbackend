﻿namespace Cargo4You.Infrastructure.Interfaces
{
    public interface IUnitOfWork
    {
        IDeliveryOptionRepository DeliveryOptionRepository { get; }
        IDeliveryPartnerRepository DeliveryPartnerRepository { get; }
        IDeliveryStateRepository DeliveryStateRepository { get; }
        IPackageHistoryRepository PackageHistoryRepository { get; }
        IPackageInfoRepository PackageInfoRepository { get; }
        IPackageRepository PackageRepository { get; }

        void Save();
    }
}