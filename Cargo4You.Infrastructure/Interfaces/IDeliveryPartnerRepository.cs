﻿using Cargo4You.Domain.Models;

namespace Cargo4You.Infrastructure.Interfaces
{
    public interface IDeliveryPartnerRepository : IRepositoryBase<DeliveryPartner>
    {
    }
}