﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Infrastructure.Repositories
{
    public class PackageInfoRepository : RepositoryBase<PackageInfo>, IPackageInfoRepository
    {
        public PackageInfoRepository(AppDbContext context) : base(context)
        {
        }
    }
}