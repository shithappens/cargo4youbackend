﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Infrastructure.Repositories
{
    public class DeliveryStateRepository : RepositoryBase<DeliveryState>, IDeliveryStateRepository
    {
        public DeliveryStateRepository(AppDbContext context) : base(context)
        {
        }
    }
}