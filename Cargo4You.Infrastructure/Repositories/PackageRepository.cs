﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cargo4You.Infrastructure.Repositories
{
    public class PackageRepository : RepositoryBase<Package>, IPackageRepository
    {
        public PackageRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Package> GetByEncodedIdAsync(Guid encodedId)
        {
            return await dbContext.Set<Package>().AsNoTracking().FirstAsync(c => c.EncodedId == encodedId);
        }
    }
}