﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Infrastructure.Repositories
{
    public class DeliveryOptionRepository : RepositoryBase<DeliveryOption>, IDeliveryOptionRepository
    {
        public DeliveryOptionRepository(AppDbContext context) : base(context)
        {
        }
    }
}