﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Infrastructure.Repositories
{
    public class DeliveryPartnerRepository : RepositoryBase<DeliveryPartner>, IDeliveryPartnerRepository
    {
        public DeliveryPartnerRepository(AppDbContext context) : base(context)
        {
        }
    }
}