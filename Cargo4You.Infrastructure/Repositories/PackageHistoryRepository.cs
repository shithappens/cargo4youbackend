﻿using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Infrastructure.Repositories
{
    public class PackageHistoryRepository : RepositoryBase<PackageHistory>, IPackageHistoryRepository
    {
        public PackageHistoryRepository(AppDbContext context) : base(context)
        {
        }
    }
}