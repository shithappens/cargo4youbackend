﻿using Cargo4You.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Cargo4You.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public DbSet<DeliveryOption> DeliveryOptions { get; set; }
        public DbSet<DeliveryState> DeliveryStates { get; set; }
        public DbSet<PackageHistory> PackageHistories { get; set; }
        public DbSet<DeliveryPartner> DeliveryPartners { get; set; }
        public DbSet<PackageInfo> PackageInfos { get; set; }
        public DbSet<Package> Packages { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(
                Assembly.GetExecutingAssembly(),
                t => t.GetInterfaces().Any(i =>
                            i.IsGenericType &&
                            i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>) &&
                            typeof(BaseEntity).IsAssignableFrom(i.GenericTypeArguments[0]))
            );
            base.OnModelCreating(builder);
        }
    }
}