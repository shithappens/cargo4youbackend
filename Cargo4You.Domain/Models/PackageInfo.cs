﻿namespace Cargo4You.Domain.Models
{
    public class PackageInfo : BaseEntity
    {
        public int PackageId { get; set; }
        public Package Package { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? PostalCode { get; set; }
        public string? Country { get; set; }
    }
}