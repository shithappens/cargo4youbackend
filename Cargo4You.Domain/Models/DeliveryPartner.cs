﻿namespace Cargo4You.Domain.Models
{
    public class DeliveryPartner : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<DeliveryOption> DeliveryOptions { get; set; }
    }
}