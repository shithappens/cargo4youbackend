﻿using System;

namespace Cargo4You.Domain.Models
{
    public class PackageHistory : BaseEntity
    {
        public int PackageId { get; set; }
        public virtual Package Package { get; set; }
        public int DeliveryStateId { get; set; }
        public virtual DeliveryState DeliveryState { get; set; }
        public DateTime TrackingDateTime { get; set; }
    }
}