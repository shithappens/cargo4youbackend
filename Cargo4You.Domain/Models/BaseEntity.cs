﻿namespace Cargo4You.Domain.Models
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}