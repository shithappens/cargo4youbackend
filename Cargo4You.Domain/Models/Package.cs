﻿namespace Cargo4You.Domain.Models
{
    public class Package : BaseEntity
    {
        public Guid EncodedId { get; set; }
        public string? Note { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public double? Length { get; set; }
        public double? Weight { get; set; }
        public double? Price { get; set; }
        public int? SenderId { get; set; }
        public virtual PackageInfo Sender { get; set; }
        public int? RecipientId { get; set; }
        public virtual PackageInfo Recipient { get; set; }
        public string? DeliveryPartnerRef { get; set; }
        public DateTime? PayedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public int? DeliveryOptionId { get; set; }

        public virtual DeliveryOption DeliveryOption { get; set; }

        public virtual ICollection<DeliveryOption> AvailableDeliveryOptions
        {
            get; set;
        }

        public virtual ICollection<PackageHistory> PackageHistories { get; set; }
    }
}