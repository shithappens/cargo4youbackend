﻿namespace Cargo4You.Domain.Models
{
    public class DeliveryOption : BaseEntity
    {
        public int DeliveryPartnerId { get; set; }
        public virtual DeliveryPartner? DeliveryPartner { get; set; }
        public double Price { get; set; }
        public int PackageId { get; set; }
        public virtual Package? Package { get; set; }
    }
}