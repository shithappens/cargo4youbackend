﻿namespace Cargo4You.Domain.Models
{
    public class DeliveryState : BaseEntity
    {
        public string Name { get; set; }
        public int StatusCode { get; set; }
        public virtual ICollection<PackageHistory> PackageHistories { get; set; }
    }
}