﻿using AutoMapper;
using Cargo4You.Application.Models;
using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.DeliveryPartner;
using Cargo4You.Application.Models.DeliveryState;
using Cargo4You.Application.Models.Package;
using Cargo4You.Application.Models.PackageHistory;
using Cargo4You.Application.Models.PackageInfo;
using Cargo4You.Domain.Models;

namespace Cargo4You.Application.Mappers

{
    public class DeliveryDTOProfile : Profile
    {
        public DeliveryDTOProfile()
        {
            CreateMap<BaseEntityDTO, BaseEntity>().ReverseMap();
            CreateMap<DeliveryOptionDTO, DeliveryOption>().ReverseMap();
            CreateMap<DeliveryPartnerDTO, DeliveryPartner>().ReverseMap();
            CreateMap<DeliveryStateDTO, DeliveryState>().ReverseMap();
            CreateMap<PackageDTO, Package>().ReverseMap();
            CreateMap<PackageHistoryDTO, PackageHistory>().ReverseMap();
            CreateMap<PackageInfoDTO, PackageInfo>().ReverseMap();
            CreateMap<Package, GetDeliveryOptionsDTO>().ReverseMap();
        }
    }
}