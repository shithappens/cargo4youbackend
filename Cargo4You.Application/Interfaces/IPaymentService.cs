﻿namespace Cargo4You.Application.Interfaces
{
    public interface IPaymentService
    {
        void PayPackage(Guid packageId);
    }
}