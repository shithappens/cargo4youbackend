﻿using Cargo4You.Application.Models.DeliveryOption;

namespace Cargo4You.Application.Interfaces
{
    public interface IDeliveryPartnerService
    {
        List<DeliveryOptionDTO> GetDeliveryOptions(GetDeliveryOptionsDTO getDeliveryOptionsDTO);
    }
}