﻿using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.Package;
using Cargo4You.Application.Models.PackageHistory;
using Cargo4You.Application.Models.PackageInfo;

namespace Cargo4You.Application.Interfaces
{
    /// <summary>
    /// All the actions related to the package itself are handled here
    /// We could use mediatr for the inputs but I didn't have time to do it
    /// </summary>
    public interface IPackageService
    {
        PackageDTO CreatePackage();

        Task<List<DeliveryOptionDTO>> GetDeliveryOptionsAsync(Guid encodedOrderId, bool isMostExpensive);

        Task SetDeliveryOptionAsync(Guid encodedOrderId, int deliveryOptionId);

        Task UpdatePackageInfoAsync(Guid encodedOrderId, PackageInfoDTO packageInfoDTO);

        Task<PackageDTO> UpdatePackageAsync(UpdatePackageDTO updatePackageDTO);

        Task<PackageDTO> GetPackageAsync(Guid encodedOrderId);

        Task<List<PackageHistoryDTO>> GetPackageHistoriesAsync(Guid encodedOrderId);

        Task<PackageInfoDTO> GetRecipientInfoAsync(Guid encodedOrderId);

        Task<PackageInfoDTO> GetSenderInfoAsync(Guid encodedOrderId);
    }
}