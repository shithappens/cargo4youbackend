﻿using AutoMapper;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.Package;
using Cargo4You.Application.Models.PackageHistory;
using Cargo4You.Application.Models.PackageInfo;
using Cargo4You.Domain.Models;
using Cargo4You.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cargo4You.Application.Services
{
    public class PackageService : IPackageService
    {
        //We could have used the strategy pattern to choose a deliveryPartner to use, but I did it like this, like that we have access to all strategies
        private readonly IEnumerable<IDeliveryPartnerService> deliveryPartnerServices;

        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PackageService(IUnitOfWork unitOfWork, IEnumerable<IDeliveryPartnerService> deliveryPartnerServices, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.deliveryPartnerServices = deliveryPartnerServices;
            this.mapper = mapper;
        }

        public PackageDTO CreatePackage()
        {
            Package package = new Package();
            package.EncodedId = Guid.NewGuid();
            package.CreatedAt = DateTime.UtcNow;
            package.ModifiedAt = DateTime.UtcNow;
            unitOfWork.PackageRepository.Create(package);
            unitOfWork.Save();
            var recipient = new PackageInfo { PackageId = package.Id };
            var sender = new PackageInfo { PackageId = package.Id };
            //package.PackageHistories = new List<PackageHistory> { new PackageHistory { DeliveryStateId = 1, TrackingDateTime = DateTime.UtcNow } };
            unitOfWork.PackageInfoRepository.Create(recipient);
            unitOfWork.PackageInfoRepository.Create(sender);
            unitOfWork.Save();
            package.RecipientId = recipient.Id;
            package.SenderId = sender.Id;
            unitOfWork.PackageRepository.Update(package);
            unitOfWork.Save();
            return mapper.Map<PackageDTO>(package);
        }

        public async Task<List<DeliveryOptionDTO>> GetDeliveryOptionsAsync(Guid encodedOrderId, bool isMostExpensive)
        {
            List<DeliveryOption> deliveryOptions = new List<DeliveryOption>();
            List<DeliveryOptionDTO> deliveryPartnerOptions = new List<DeliveryOptionDTO>();
            List<DeliveryOptionDTO> choosenDeliveryOptions = new List<DeliveryOptionDTO>();
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            var request = mapper.Map<GetDeliveryOptionsDTO>(package);
            DeliveryOptionDTO? mostExpensiveOption = null;
            foreach (var deliveryPartner in deliveryPartnerServices)
            {
                deliveryPartnerOptions = deliveryPartner.GetDeliveryOptions(request);
                if (isMostExpensive)
                {
                    mostExpensiveOption = deliveryPartnerOptions.MaxBy(deliveryOption => deliveryOption.Price);
                    if (mostExpensiveOption != null)
                    {
                        choosenDeliveryOptions.Add(mostExpensiveOption);
                    }
                }
                else
                {
                    choosenDeliveryOptions.AddRange(deliveryPartnerOptions);
                }
            }
            package.AvailableDeliveryOptions = mapper.Map<List<DeliveryOption>>(choosenDeliveryOptions);
            package.DeliveryOption = null;
            unitOfWork.PackageRepository.Update(package);
            unitOfWork.Save();
            return mapper.Map<List<DeliveryOptionDTO>>(package.AvailableDeliveryOptions);
        }

        public async Task<PackageDTO> GetPackageAsync(Guid encodedOrderId)
        {
            return mapper.Map<PackageDTO>(await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId));
        }

        public async Task<List<PackageHistoryDTO>> GetPackageHistoriesAsync(Guid encodedOrderId)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            return await mapper.ProjectTo<PackageHistoryDTO>(unitOfWork.PackageHistoryRepository.FindByCondition(ph => ph.PackageId == package.Id)).ToListAsync();
        }

        public async Task SetDeliveryOptionAsync(Guid encodedOrderId, int deliveryOptionId)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            package.DeliveryOptionId = deliveryOptionId;
            unitOfWork.PackageRepository.Update(package);
            unitOfWork.Save();
        }

        public async Task<PackageDTO> UpdatePackageAsync(UpdatePackageDTO updatePackageDTO)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(updatePackageDTO.EncodedId);
            package.Length = updatePackageDTO.Length;
            package.Width = updatePackageDTO.Width;
            package.Weight = updatePackageDTO.Weight;
            package.Height = updatePackageDTO.Height;
            package.ModifiedAt = DateTime.UtcNow;
            package.DeliveryOptionId = updatePackageDTO.DeliveryOptionId == 0 ? package.DeliveryOptionId : updatePackageDTO.DeliveryOptionId;
            unitOfWork.PackageRepository.Update(package);
            unitOfWork.Save();
            await this.GetDeliveryOptionsAsync(package.EncodedId, true);
            return mapper.Map<PackageDTO>(package);
        }

        public async Task UpdatePackageInfoAsync(Guid encodedOrderId, PackageInfoDTO packageInfoDTO)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            packageInfoDTO.PackageId = package.Id;
            if (package.Id == packageInfoDTO.PackageId)
            {
                unitOfWork.PackageInfoRepository.Update(mapper.Map<PackageInfo>(packageInfoDTO));
                unitOfWork.Save();
            }
        }

        public async Task CreatePackageInfo(Guid encodedOrderId, PackageInfoDTO packageInfoDTO, bool isRecipient)
        {
            var packageInfo = mapper.Map<PackageInfo>(packageInfoDTO);
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            packageInfo.PackageId = package.Id;
            unitOfWork.PackageInfoRepository.Create(packageInfo);
            unitOfWork.Save();
            if (isRecipient)
            {
                package.RecipientId = packageInfo.Id;
            }
            else
            {
                package.SenderId = packageInfo.Id;
            }
            unitOfWork.Save();
        }

        public async Task<PackageInfoDTO> GetRecipientInfoAsync(Guid encodedOrderId)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            return mapper.Map<PackageInfoDTO>(unitOfWork.PackageInfoRepository.FindById(package.RecipientId));
        }

        public async Task<PackageInfoDTO> GetSenderInfoAsync(Guid encodedOrderId)
        {
            var package = await unitOfWork.PackageRepository.GetByEncodedIdAsync(encodedOrderId);
            return mapper.Map<PackageInfoDTO>(unitOfWork.PackageInfoRepository.FindById(package.SenderId));
        }
    }
}