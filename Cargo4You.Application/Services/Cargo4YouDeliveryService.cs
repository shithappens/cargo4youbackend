﻿using AutoMapper;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.DeliveryPartner;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Application.Services
{
    public class Cargo4YouDeliveryService : IDeliveryPartnerService
    {
        private const string Name = "Cargo4You";
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public Cargo4YouDeliveryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<DeliveryOptionDTO> GetDeliveryOptions(GetDeliveryOptionsDTO request)
        {
            var deliveryPartner = unitOfWork.DeliveryPartnerRepository.FindByCondition(c => c.Name == Name).First();
            var deliveryPartnerDto = mapper.Map<DeliveryPartnerDTO>(deliveryPartner);
            List<DeliveryOptionDTO> deliveryOptions = new List<DeliveryOptionDTO>();
            // We could put this in an helper class / doing it while mapping the GetDeliveryOptionApi to GetDeliveryOptionDTO
            var cubicSize = request.Height * request.Width * request.Length;
            if (request.Weight <= 20)
            {
                if (request.Weight <= 2)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 15 });
                }
                else if (request.Weight <= 15)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 18 });
                }
                else
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 35 });
                }
            }
            if (cubicSize <= 2000)
            {
                if (cubicSize <= 1000)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 10 });
                }
                else
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 20 });
                }
            }
            return deliveryOptions;
        }
    }
}