﻿using AutoMapper;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.DeliveryPartner;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Application.Services
{
    public class MaltaShipService : IDeliveryPartnerService
    {
        private const string Name = "MaltaShip";

        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public MaltaShipService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<DeliveryOptionDTO> GetDeliveryOptions(GetDeliveryOptionsDTO request)
        {
            var deliveryPartner = unitOfWork.DeliveryPartnerRepository.FindByCondition(c => c.Name == Name).First();
            var deliveryPartnerDto = mapper.Map<DeliveryPartnerDTO>(deliveryPartner);
            List<DeliveryOptionDTO> deliveryOptions = new List<DeliveryOptionDTO>();
            // We could put this in an helper class / doing it while mapping the GetDeliveryOptionApi to GetDeliveryOptionDTO
            var cubicSize = request.Height * request.Width * request.Length;
            if (request.Weight >= 10)
            {
                if (request.Weight <= 20)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 16.99 });
                }
                else if (request.Weight <= 30)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 33.99 });
                }
                else
                {
                    var tmpDeliveryOption = new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 43.99 };
                    var tmpWeight = request.Weight - 25;
                    if (tmpWeight > 0)
                    {
                        tmpDeliveryOption.Price += tmpWeight * 0.41;
                    }
                    deliveryOptions.Add(tmpDeliveryOption);
                }
            }
            if (cubicSize >= 500)
            {
                if (cubicSize <= 1000)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 9.50 });
                }
                else if (cubicSize <= 2000)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 19.50 });
                }
                else if (cubicSize <= 5000)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 48.50 });
                }
                else
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 147.50 });
                }
            }
            return deliveryOptions;
        }
    }
}