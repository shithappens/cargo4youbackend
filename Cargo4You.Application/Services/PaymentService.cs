﻿using Cargo4You.Application.Interfaces;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Application.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IUnitOfWork unitOfWork;

        public PaymentService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public void PayPackage(Guid packageId)
        {
            // There is a whole payement flow to do, but here let's say we have a callback and it provides the package id
            var package = unitOfWork.PackageRepository.FindByCondition(p => p.EncodedId == packageId).FirstOrDefault();
            if (package != null)
            {
                package.PayedAt = DateTime.Now;
                unitOfWork.PackageRepository.Update(package);
                unitOfWork.Save();
            }
        }
    }
}