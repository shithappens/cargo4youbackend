﻿using AutoMapper;
using Cargo4You.Application.Interfaces;
using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.DeliveryPartner;
using Cargo4You.Infrastructure;
using Cargo4You.Infrastructure.Interfaces;

namespace Cargo4You.Application.Services
{
    public class ShipFasterService : IDeliveryPartnerService
    {
        private const string Name = "ShipFaster";
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ShipFasterService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public List<DeliveryOptionDTO> GetDeliveryOptions(GetDeliveryOptionsDTO request)
        {
            var deliveryPartner = unitOfWork.DeliveryPartnerRepository.FindByCondition(c => c.Name == Name).First();
            var deliveryPartnerDto = mapper.Map<DeliveryPartnerDTO>(deliveryPartner);
            List<DeliveryOptionDTO> deliveryOptions = new List<DeliveryOptionDTO>();
            // We could put this in an helper class / doing it while mapping the GetDeliveryOptionApi to GetDeliveryOptionDTO
            var cubicSize = request.Height * request.Width * request.Length;
            if (request.Weight > 10 && request.Weight <= 30)
            {
                if (request.Weight <= 15)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 16.50 });
                }
                else if (request.Weight <= 25)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 36.50 });
                }
                else
                {
                    var tmpDeliveryOption = new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 40 };
                    var tmpWeight = request.Weight - 25;
                    if (tmpWeight > 0)
                    {
                        tmpDeliveryOption.Price += tmpWeight * 0.417;
                    }
                    deliveryOptions.Add(tmpDeliveryOption);
                }
            }
            if (cubicSize <= 1700)
            {
                if (cubicSize <= 1000)
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 11.99 });
                }
                else
                {
                    deliveryOptions.Add(new DeliveryOptionDTO { DeliveryPartnerId = deliveryPartner.Id, Price = 21.99 });
                }
            }
            return deliveryOptions;
        }
    }
}