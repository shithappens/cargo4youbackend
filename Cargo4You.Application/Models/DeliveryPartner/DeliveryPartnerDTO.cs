﻿using Cargo4You.Application.Models.DeliveryOption;

namespace Cargo4You.Application.Models.DeliveryPartner
{
    public class DeliveryPartnerDTO : BaseEntityDTO
    {
        public string Name { get; set; }
        public virtual ICollection<DeliveryOptionDTO> DeliveryOptions { get; set; }
    }
}