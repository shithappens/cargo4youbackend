﻿using Cargo4You.Application.Models.Package;

namespace Cargo4You.Application.Models.PackageInfo
{
    public class PackageInfoDTO : BaseEntityDTO
    {
        public int? PackageId { get; set; }
        public virtual PackageDTO? Package { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? PostalCode { get; set; }
        public string? Country { get; set; }
    }
}