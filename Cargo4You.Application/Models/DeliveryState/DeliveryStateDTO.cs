﻿using Cargo4You.Application.Models.PackageHistory;

namespace Cargo4You.Application.Models.DeliveryState
{
    public class DeliveryStateDTO : BaseEntityDTO
    {
        public string Name { get; set; }
        public int StatusCode { get; set; }
        public virtual ICollection<PackageHistoryDTO> PackageHistories { get; set; }
    }
}