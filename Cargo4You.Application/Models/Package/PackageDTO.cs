﻿using Cargo4You.Application.Models.DeliveryOption;
using Cargo4You.Application.Models.PackageHistory;

namespace Cargo4You.Application.Models.Package
{
    public class PackageDTO : BaseEntityDTO
    {
        public Guid EncodedId { get; set; }
        public string Note { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public double Weight { get; set; }
        public double Price { get; set; }
        public string DeliveryPartnerRef { get; set; }
        public DateTime PayedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public DateTime DeletedAt { get; set; }
        public int DeliveryOptionId { get; set; }
    }
}