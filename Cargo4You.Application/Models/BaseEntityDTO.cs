﻿namespace Cargo4You.Application.Models
{
    public class BaseEntityDTO
    {
        public int Id { get; set; }
    }
}