﻿using Cargo4You.Application.Models.DeliveryPartner;
using Cargo4You.Application.Models.Package;

namespace Cargo4You.Application.Models.DeliveryOption
{
    public class DeliveryOptionDTO : BaseEntityDTO
    {
        public int DeliveryPartnerId { get; set; }
        public virtual DeliveryPartnerDTO DeliveryPartner { get; set; }
        public double Price { get; set; }
        public int PackageId { get; set; }
        public virtual PackageDTO Package { get; set; }
    }
}