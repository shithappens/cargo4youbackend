﻿namespace Cargo4You.Application.Models.DeliveryOption
{
    public class GetDeliveryOptionsDTO
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public double Weight { get; set; }
    }
}