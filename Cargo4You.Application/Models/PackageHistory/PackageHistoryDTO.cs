﻿using Cargo4You.Application.Models.DeliveryState;
using Cargo4You.Application.Models.Package;

namespace Cargo4You.Application.Models.PackageHistory
{
    public class PackageHistoryDTO : BaseEntityDTO
    {
        public int PackageId { get; set; }
        public virtual PackageDTO Package { get; set; }
        public int DeliveryStateId { get; set; }
        public virtual DeliveryStateDTO DeliveryState { get; set; }
        public DateTime TrackingDateTime { get; set; }
    }
}